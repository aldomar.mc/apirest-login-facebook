package com.zeytol.login_facebook.utils;

public class Constant {
    //Url donde se aloja nuestra API LoginFacebook
    public static final String URL_BASE = "http://localhost:8081/login_facebook";
    //Url externo que al iniciar sesión se debe redirigir al home de la app
    public static final String URL_EXTERNO = "http://localhost:8080/home";
}