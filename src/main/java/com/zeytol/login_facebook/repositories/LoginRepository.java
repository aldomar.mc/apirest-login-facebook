package com.zeytol.login_facebook.repositories;

public interface LoginRepository {

    public String generateFacebookAuthorizeUrl();
    public void generateFacebookAccessToken(String code);
    public String getUserData();
}
