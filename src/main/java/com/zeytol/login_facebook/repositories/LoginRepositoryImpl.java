package com.zeytol.login_facebook.repositories;

import com.zeytol.login_facebook.utils.Constant;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.social.facebook.connect.FacebookConnectionFactory;
import org.springframework.social.oauth2.OAuth2Parameters;
import org.springframework.stereotype.Repository;

@Repository
public class LoginRepositoryImpl implements LoginRepository {
    private String accessToken;

    // REDIREC_URI: Url para generar el token de acceso a Facebook
    private static final String REDIREC_URI = Constant.URL_BASE + "/generateFacebookAccessToken";

    @Value("${spring.social.facebook.app-id}")
    private String facebookAppId;

    @Value("${spring.social.facebook.app-secret}")
    private String facebookAppSecret;

    private FacebookConnectionFactory createConnection() {
        return new FacebookConnectionFactory(facebookAppId, facebookAppSecret);
    }

    /*
     * Generamos la url de autorización
     * */
    @Override
    public String generateFacebookAuthorizeUrl() {
        OAuth2Parameters parameters = new OAuth2Parameters();
        parameters.setRedirectUri(REDIREC_URI);
        parameters.setScope("email");
        return createConnection().getOAuthOperations().buildAuthenticateUrl(parameters);
    }

    /*
     * Generamos el token de acceso
     * */
    @Override
    public void generateFacebookAccessToken(String code) {
        accessToken = createConnection().getOAuthOperations().exchangeForAccess(code, REDIREC_URI, null)
                .getAccessToken();
    }

    /*
     * Obtenemos la información básica del usuario
     * */
    @Override
    public String getUserData() {
        Facebook facebook = new FacebookTemplate(accessToken);
        String[] fields = {"id", "first_name", "name", "email", "birthday", "gender", "age_range", "hometown",
                "inspirational_people"};
        return facebook.fetchObject("me", String.class, fields);
    }


}
