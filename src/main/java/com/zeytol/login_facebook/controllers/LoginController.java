package com.zeytol.login_facebook.controllers;

import com.zeytol.login_facebook.services.LoginService;
import com.zeytol.login_facebook.utils.Constant;
import com.zeytol.login_facebook.utils.General;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@RestController
public class LoginController {
    @Autowired
    private LoginService loginService;
    @Autowired
    private General general;

    private static final String URL_EXTERNO = Constant.URL_EXTERNO;
    private static String urlBase = "";

    @GetMapping
    public List<String> welcome(HttpServletRequest request) {
        urlBase = general.getFullURL(request);
        List<String> urls = new ArrayList<>();
        urls.add(urlBase + "generateFacebookAuthorizeUrl");
        urls.add(urlBase + "getUserData");
        urls.add(urlBase + "getUserFeed");
        urls.add(urlBase + "getUserAlbum");
        urls.add(urlBase + "getUserAccounts");
        urls.add(urlBase + "getUserGroupMembership");
        return urls;
    }

    @GetMapping("/generateFacebookAuthorizeUrl")
    public String generateFacebookAuthorizeUrl() {
        return loginService.generateFacebookAuthorizeUrl();
    }

    @GetMapping("/generateFacebookAccessToken")
    public ModelAndView generateFacebookAccessToken(@RequestParam("code") String code) {
        loginService.generateFacebookAccessToken(code);
        // Utilizamos ModelAndView, para redireccionar a una URL exterior
        return new ModelAndView("redirect:" + URL_EXTERNO);
    }

    @GetMapping("/getUserData")
    public String getUserData() {
        return loginService.getUserData();
    }

}
