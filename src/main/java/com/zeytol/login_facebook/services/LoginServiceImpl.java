package com.zeytol.login_facebook.services;

import com.zeytol.login_facebook.repositories.LoginRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginServiceImpl implements LoginService {
    @Autowired
    private LoginRepository loginRepository;

    @Override
    public String generateFacebookAuthorizeUrl() {
        return loginRepository.generateFacebookAuthorizeUrl();
    }

    @Override
    public void generateFacebookAccessToken(String code) {
        loginRepository.generateFacebookAccessToken(code);
    }

    @Override
    public String getUserData() {
        return loginRepository.getUserData();
    }
}
