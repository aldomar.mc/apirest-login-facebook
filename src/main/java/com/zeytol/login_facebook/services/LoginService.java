package com.zeytol.login_facebook.services;

public interface LoginService {
    public String generateFacebookAuthorizeUrl();
    public void generateFacebookAccessToken(String code);
    public String getUserData();
}
