package com.zeytol.login_facebook;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LoginFacebookApplication {

    public static void main(String[] args) {
        SpringApplication.run(LoginFacebookApplication.class, args);
    }

}
