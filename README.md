**ApiRest login facebook**

El api se debe consumir desde cualquier otro proyecto, su finalidad es crear una sesión con facebook

**Herramientas**

Está desarrollado usando las siguientes herramientas:

* Framework Spring Boot (spring-social-facebook)
* IDE Intellij idea
* Java 11
* Tomcat 8.5

**Importante**
Debe crear un app en la plataforma de Facebook Developers

**Contacto:** 
aomc@outlook.es / aomc06@gmail.com